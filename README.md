# Projet Hilbert Au delà du réel

Cet espace peut servir à partager des références, ou du travail ultérieur.

## Documents externes:

- Le compte rendu de la réunion du 16-17 juin 2022: https://codimd.math.cnrs.fr/nJ5r2fTKQ_ubCCsdn4p4DA?both, avec une ébauche de projet (en cours de rédaction).
